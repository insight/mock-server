
const http = require('http');
const https = require('https');
const url=require("url");
const pass = require('stream').PassThrough;

const getHeader = function (req) {
    let ret = {};
    for (let i in req.headers) {
        if (!/^(host|connection|Access-|origin|referer|user-agent|user-mocker|path-mocker|url-mocker|method-mocker|headers-mocker|X-Requested-With)/i.test(i)) {
            ret[i] = req.headers[i];
        }
    }
    ret["accept"]="*/*";
    let headers=req.headers["headers-mocker"];
    if(headers)
    {
        headers=JSON.parse(headers);
        for(let key in headers)
        {
            ret[key]=headers[key];
        }
    }
    return ret
};

const filterResHeader = function (headers,res) {
    let ret = {};
    res.setHeader("Cache-Control", "no-cache,no-store");
    res.setHeader("Pragrma", "no-cache");
    res.setHeader("Expires", 0);
    let resHeaders=res.getHeader("Access-Control-Expose-Headers")?res.getHeader("Access-Control-Expose-Headers").toLowerCase():"";
    for (let i in headers) {
        if (!/Access-/i.test(i)) {
            if(/set-cookie/i.test(i))
            {
                for(let index=0;index<headers[i].length;index++)
                {
                    headers[i][index]=headers[i][index].split(" ")[0];
                }
                ret[i]=headers[i];
            }
            else
            {
                ret[i] = headers[i];
            }
        }
        if(!resHeaders)
        {
            res.setHeader("Access-Control-Expose-Headers",i);
        }
        else if(resHeaders.indexOf(i.toLowerCase()+",")==-1 && resHeaders.indexOf(","+i.toLowerCase())==-1)
        {
            res.setHeader("Access-Control-Expose-Headers",res.getHeader("Access-Control-Expose-Headers")+","+i);
        }
    }
    return ret;
};

const getPath = function (req) {
    let url = req.url;
    if (url.substr(0, 7).toLowerCase() === 'http://') {
        let i = url.indexOf('/', 7);
        if (i !== -1) {
            url = url.substr(i);
        }
    }
    return url;
};

function getHost(req) {
    let url=req.headers["url-mocker"];
    url=url.replace(/^(http:\/\/|https:\/\/)/i,"");
    let arr=url.split(":");
    return arr[0];
}

function getPort(req) {
    let url=req.headers["url-mocker"];
    let defaultPort;
    if(req.headers["url-mocker"].toLowerCase().startsWith("https://"))
    {
        defaultPort=443;
    }
    else
    {
        defaultPort=80;
    }
    url=url.replace(/^(http:\/\/|https:\/\/)/i,"");
    let arr=url.split(":");
    return arr.length>1?arr[1]:defaultPort;
}

const onProxy = function (req, res) {
    handleHeader(req,res);
    if(req.method=="OPTIONS")
    {
        return;
    }
    if(!req.headers["url-mocker"])
    {
        mock(req,res);
    }
    else
    {
        proxy(req,res);
    }
};

function mock(req,res) {
    let obj=url.parse(mockUrl);
    let path=req.headers.path || url.parse(req.url).path;
    let opt={
        host:     obj.hostname,
        path:     (obj.pathname=="/"?"":obj.pathname)+path,
        method:   req.method,
        headers:  getHeader(req),
        port:obj.port?obj.port:80,
    }
    console.log("初次请求：method:"+opt.method+" host:"+opt.host+" port:"+opt.port+" path:"+opt.path)
    if(opt.headers["content-length"])
    {
        delete opt.headers["content-length"]
    }
    let req2 = http.request(opt, function (res2) {
        if(!realUrl || res2.headers["finish-mocker"]=="0")
        {
            console.log("接口开发中，返回mock数据");
            res.writeHead(res2.statusCode, filterResHeader(res2.headers,res));
            res2.pipe(res);
            res2.on('end', function () {

            });
        }
        else
        {
            if(res2.headers["finish-mocker"]=="1")
            {
                console.log("接口已完成，调用真实接口");
            }
            else
            {
                console.log("接口或者项目未找到，转调真实接口");
            }
            let headers=getHeader(req);
            let objUrl=url.parse(realUrl);
            let request1,opt1;
            if(objUrl.protocol=="http:")
            {
                opt1={
                    host:  objUrl.hostname,
                    path:     (objUrl.pathname=="/"?"":objUrl.pathname)+path,
                    method:   req.method,
                    port:objUrl.port?objUrl.port:80,
                    headers:headers
                }
                request1=http.request;
            }
            else
            {
                opt1={
                    host:  objUrl.hostname,
                    path:     (objUrl.pathname=="/"?"":objUrl.pathname)+path,
                    method:   req.method,
                    port:objUrl.port?objUrl.port:443,
                    headers:headers,
                    rejectUnauthorized: false,
                    requestCert: true,
                }
                request1=https.request;
            }
            console.log("调用真实接口：method:"+opt1.method+"host:"+opt1.host+"port:"+opt1.port+"path:"+opt1.path)
            let req3=request1(opt1,function (res3) {
                console.log("真实接口调用完成。status:"+res3.statusCode)
                res.writeHead(res3.statusCode, filterResHeader(res3.headers,res));
                res3.pipe(res);
                res3.on('end', function () {

                });
            })
            if (/POST|PUT/i.test(req.method)) {
                stream.pipe(req3);
            } else {
                req3.end();
            }
            req3.on('error', function (err) {
                res.end(err.stack);
            });
        }
    });
    let stream;
    if (/POST|PUT|PATCH/i.test(req.method))
    {
        stream=new pass();
        req.pipe(stream);
        req.pipe(req2);
    }
    else
    {
        req2.end();
    }
    req2.on('error', function (err) {
        res.end(err.stack);
    });
}

function handleHeader(req,res) {
    if(!req.headers.origin)
    {
        return;
    }
    res.setHeader("Access-Control-Allow-Origin", req.headers.origin);
    res.setHeader("Access-Control-Allow-Headers", "X-Requested-With");
    res.setHeader("Access-Control-Allow-Methods","PUT,POST,GET,DELETE");
    res.setHeader("Access-Control-Allow-Credentials","true");
    if(req.headers["access-control-request-headers"])
    {
        res.setHeader("Access-Control-Allow-Headers",req.headers["access-control-request-headers"])
    }
    res.setHeader("Access-Control-Expose-Headers","connection,content-length,date,x-powered-by,content-encoding,server,etag,accept-ranges,allow,content-language,set-cookie,mocker-request");
    if(req.method=="OPTIONS")
    {
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end();
        return;
    }
}

function redirect(res,bHttps,opt,location) {
    let urlRedirect=location;
    if(urlRedirect.startsWith("/"))
    {
        urlRedirect=(bHttps?"https://":"http://")+opt.host+":"+opt.port+urlRedirect;
    }
    let objUrl=url.parse(urlRedirect);
    let request1,opt1;
    if(objUrl.protocol=="http:")
    {
        opt1={
            host:  objUrl.hostname,
            path:     objUrl.path,
            method:   "GET",
            port:objUrl.port?objUrl.port:80,
        }
        request1=http.request;
        bHttps=false;
    }
    else
    {
        opt1={
            host:  objUrl.hostname,
            path:     objUrl.path,
            method:   "GET",
            port:objUrl.port?objUrl.port:443,
            rejectUnauthorized: false,
            requestCert: true,
        }
        request1=https.request;
        bHttps=true;
    }
    let req3=request1(opt1,function (res3) {
        if(res3.statusCode==302)
        {
            redirect(res,bHttps,opt,res3.headers.location)
        }
        else
        {
            let resHeader=filterResHeader(res3.headers,res)
            resHeader["mocker-request"]=JSON.stringify(handleSelfCookie(req3));
            res.writeHead(res3.statusCode, resHeader);
            res3.pipe(res);
            res3.on('end', function () {

            });
        }
    })
    req3.end();
    req3.on('error', function (err) {
        res.end(err.stack);
    });
}

function handleSelfCookie(req) {
    let arr=req._headers;
    arr["url"]=req.method+" "+req.path;
    let cookie=arr["cookie"];
    if(!cookie)
    {
        return arr;
    }
    let arrCookie=cookie.split(";");
    let keys=["id","name","photo","qq","sex","company","phone","loginCount","age","email"];
    arrCookie=arrCookie.filter(function (obj) {
        obj=obj.trim();
        for(let key of keys)
        {
            if(obj.startsWith(key+"="))
            {
                return false;
            }
        }
        return true;
    })
    arr["cookie"]=arrCookie.join(";");
    return arr;
}

function proxy(req,res) {
    let bHttps=false;
    if(req.headers["url-mocker"].toLowerCase().startsWith("https://"))
    {
        bHttps=true;
    }
    let opt,request;
    if(bHttps)
    {
        opt= {
            host:     getHost(req),
            path:     req.headers["path-mocker"],
            method:   req.headers["method-mocker"],
            headers:  getHeader(req),
            port:getPort(req),
            rejectUnauthorized: false,
            requestCert: true,
        };
        request=https.request;
    }
    else
    {
        opt= {
            host:     getHost(req),
            path:     req.headers["path-mocker"],
            method:   req.headers["method-mocker"],
            headers:  getHeader(req),
            port:getPort(req)
        };
        request=http.request;
    }
    let req2 = request(opt, function (res2) {
        if(res2.statusCode==302)
        {
            redirect(res,bHttps,opt,res2.headers.location)
        }
        else
        {
            let resHeader=filterResHeader(res2.headers,res)
            resHeader["mocker-request"]=JSON.stringify(handleSelfCookie(req2));
            res.writeHead(res2.statusCode, resHeader);
            res2.pipe(res);
            res2.on('end', function () {

            });
        }
    });
    if (/POST|PUT|PATCH/i.test(req.method)) {
        req.pipe(req2);
    } else {
        req2.end();
    }
    req2.on('error', function (err) {
        res.end(err.stack);
    });
}

let arguments = process.argv.splice(2);
let mockUrl=arguments[0];
let realUrl=arguments[1];
let port=arguments[2]?arguments[2]:36742;
let server = http.createServer(onProxy);

server.listen(port);
console.log(arguments.length>0?("内网测试，Mock数据正监听端口："+port):"正在运行中！");
