const {dispatcher} = require('../core/controller');


const handler = (req, res, next) => {
  let app = req.app;
  let config = app.siteconfig || {};
  let content = config.content || {};
  res.render('default/index', {title: config.title, content: content['index']});
};

const path = '/';
const prefix = '/';

const controller = dispatcher().get(prefix, handler);

module.exports = {
  controller,
  path,
  prefix,
};
