const {dispatcher} = require('../../core/controller');

const prefix = '/';

/* GET users listing. */

const path = '/users';

const handler = (req, res, next) => {
  res.send('respond with a resource');
};

const controller = dispatcher().get(prefix, handler);

module.exports = {
  controller,
  path,
  prefix,
};
