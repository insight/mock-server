const {dispatcher} = require('../../core/controller');

const prefix = '/';

const path = '/openapi/goods';

const handler = function(req,res,next){
    res.send(req.loader.map());
};

const controller = dispatcher().get(prefix, handler);

module.exports = {
  controller,
   path,
  prefix,
};