const engine = require('./core/serverEngine');

const RouterLoader = require('./core/routerLoader');
const ConfigLoader = require('./core/configLoader');

/**
 * http-server
 * @param String confPath
 * @returns {*|Express}
 */
function server({confPath = './config/app'}) {
  let app = engine();
  // 全局配置
  app.siteconfig = require(confPath);
  // 路由加载器
  app.routerMap = new RouterLoader(app, app.siteconfig.apiDirs);
  // 注册自动加载 路由
  ConfigLoader(app,app.siteconfig);
  return app
}

module.exports = server({});
