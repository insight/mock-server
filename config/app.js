// 应用 配置
const path = require('path');

// 应用相关配置
const app = {
   appUrl : 'http://localhost', // 应用域名
   port : 80, // http-server 监听端口
   apiDirs : [path.join(__dirname, '../apis')], // api mock 目录
   staticDir : path.join(__dirname, '../public'), // 静态文件存储目录
   viewDir : path.join(__dirname, '../views'), // HTML模板存储目录
   loggerMode : 'dev', // 日志模式

   // app.set
   settings : {
      'view engine' : 'pug',
      'views': path.join(__dirname, '../views')
   },
   // app.use
   plugins : require('./plugins'),
   content : {
       index : "Mock Server 诺坤小茶",
   },
};

module.exports = app;