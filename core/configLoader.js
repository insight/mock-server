const _ = require('lodash');
/**
 * 配置自动加载
 * @param app
 * @param config
 */
const loader = function (app, config = {}) {

  if (_.isUndefined(app) || !_.isObject(app)) {
    return false
  }
  if (_.isEmpty(config) || !_.isObject(config)) {
    return false
  }
  let {settings = {}, plugins = []} = config || {};

  // app.set 遍历 设置
  _.forEach(settings, (val, key) => {
    app.set(key, val);
  });

  // 遍历 plugins
  _.forEach(plugins, (plugin, _index) => {
    let handler;

    if (!_.isFunction(plugin) && _.isObject(plugin)) {
      let {register} = plugin ;
      if (!_.isFunction(register)) {
        return;
      }
      handler = register(app)
    }

    if (_.isFunction(plugin)) {
      handler = plugin;
    }
    if(!_.isFunction(handler)){
       return;
    }
    app.use(handler);
  })

};

module.exports = loader;